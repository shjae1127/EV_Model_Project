z=transpose(motor_efficiency_efficiency);
y=repmat(motor_efficiency_torque,1,83);
x=transpose(motor_efficiency_RPM);
x=repmat(x,61,1);

x_line=[-8421.41102200000,8421.41102200000];
x_line2=[0,0];
y_line=[-255,255];
figure()
contourf(x,y,z,[0 40 50 60 70 80 85 90 92 93 94 95 96],'ShowText',true)
hold on
plot(x_line,x_line2,'k','LineWidth',3)
hold on
plot(x_line2,y_line,'k','LineWidth',3)
title('eff_map')
xlabel('Speed [RPM]')
ylabel('Torque [N*m]')